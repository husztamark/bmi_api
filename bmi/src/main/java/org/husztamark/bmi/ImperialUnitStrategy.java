package org.husztamark.bmi;

public class ImperialUnitStrategy implements UnitStrategy {

	@Override
	public double calculate(double weight, double height) {
		double result = Math.round(703*(weight / (Math.pow(height,2))*100.0))/100.0;
		return result;
	}

}
