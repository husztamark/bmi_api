package org.husztamark.bmi;

public interface UnitStrategy {
	public double calculate(double weight, double height);

}
