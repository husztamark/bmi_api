package org.husztamark.bmi;

public class MetricUnitStrategy implements UnitStrategy {

	@Override
	public double calculate(double weight, double height) {
		double result = Math.round(weight / (Math.pow(height,2))*100.0)/100.0;
		return result;
	}
	
}
