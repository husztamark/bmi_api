package org.husztamark.bmi;

public class Bmi {
	protected double weight;
	protected double height;
	protected UnitStrategy strategy;
	
	
	public Bmi(double weight, double height, UnitStrategy strategy ) {
		this.weight = weight;
		this.height = height;
		this.strategy = strategy;
	}
	
	public double calculate() throws IllegalArgumentException {
		if(weight > 0 && height > 0) {
			double result = strategy.calculate(weight, height);
			if(result < 16) {
				System.out.println( result + " Severe Thinness");
			}
			else if(result >= 16 && result < 17) {
				System.out.println( result +" Moderate Thinness");
			}
			else if(result >= 17 && result < 18.5) {
				System.out.println( result +" Mild Thinness");
			}
			else if(result >= 18.5 && result < 25) {
				System.out.println( result +" Normal");
			}
			else if(result >= 25 && result < 30) {
				System.out.println( result +" Overweight");
			}
			else if(result >= 30 && result < 35) {
				System.out.println( result +" Obese Class I");
			}
			else if(result >= 35 && result < 40) {
				System.out.println( result +" Obese Class II");
			}
			else if(result >= 40) {
				System.out.println( result +" Obese Class III");
			}
			return result;
		}
		else {
			throw new IllegalArgumentException("Invalid parameters");
		}
	}
}
