package org.husztamark.bmi;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class BmiTest
{
   private Bmi bmi;
   private double weight;
   private double height;
   private UnitStrategy strategy;
   
   
   @Test
   public void testMetricCalculateWhenBothParameterArePositive() {
	   weight = 70;
	   height = 1.70;
	   strategy = new MetricUnitStrategy();
	   bmi = new Bmi(weight, height, strategy);
	   double result = bmi.calculate();
	   assertEquals(24.22,result,0.01);
   }
   
   @Test(expected = IllegalArgumentException.class)
   public void testMetricCalculateWhenFirstParameterInvalid() {
	   weight = -1;
	   height = 1.6;
	   strategy = new MetricUnitStrategy();
	   bmi = new Bmi(weight, height, strategy);
	   bmi.calculate();
   }
   
   @Test(expected = IllegalArgumentException.class)
   public void testMetricCalculateWhenSecondParameterInvalid() {
	   weight = 100;
	   height = -5;
	   strategy = new MetricUnitStrategy();
	   bmi = new Bmi(weight, height, strategy);
	   bmi.calculate();
   }
   
   @Test
   public void testImperialCalculateWhenBothParameterArePositive() {
	   weight = 160;
	   height = 70;
	   strategy = new ImperialUnitStrategy();
	   bmi = new Bmi(weight, height, strategy);
	   double result = bmi.calculate();
	   assertEquals(22.96,result,0.01);
   }
   
   
   
   

}
